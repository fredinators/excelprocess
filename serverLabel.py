# -*-coding: utf-8 -*-
"""
# @Time:          2021-09-27
# @Author:        wubangying
# @Description:   create label of servers
"""
from excelComm import *
from func import *
my_file = r'a.xlsx'
oriFile = xlrd.open_workbook(my_file)
print(oriFile.sheet_names())
# 读取数据信息表单
sheet1 = oriFile.sheet_by_name('a')
# value = sheet1.cell_value(10,3)
rowMax = sheet1.nrows
# 表格字段
field = sheet1.row_values(2)

# 获取所需的表格字段索引
eqpNO = field.index('设备编号')
business = field.index('业务名称')
IP = field.index('内部IP')
CPU1 = field.index('CPU型号及数量')
CPU2 = field.index('CPU大小')
storage1 = field.index('内存数量')
storage2 = field.index('内存大小')
storage3 = field.index('服务器硬盘空间')
OS = field.index('操作系统')

# 获取表格字段整列对应的单元格内容my_dict = sheet1.col_values(eqpNO)
my_dict = {'设备编号': sheet1.col_values(eqpNO), '业务名称/IP': combination(sheet1.col_values(business), sheet1.col_values(IP)),
            '单位/部门':genlst('数据中心', rowMax), '物理资源': combination2(sheet1.col_values(CPU1), sheet1.col_values(CPU2),sheet1.col_values(storage1),
                                                 sheet1.col_values(storage2), sheet1.col_values(storage3)),
            '备注':sheet1.col_values(OS)}


tarFile = Workbook(encoding='utf-8')
tarSheet = tarFile.add_sheet('a')
eqpCount = -1
busCount = -1
depaCount = -1
phyCount = -1
noteCount = -1
for rowNo in range(0, int(math.ceil(rowMax/3))*6):
    for colNo in range(0, 8):
        if rowNo%6 == 0:
            if colNo%3 == 0:
                tarSheet.write(rowNo, colNo, '设备编号')
            if colNo%3 == 1:
                eqpCount += 1
                tarSheet.write(rowNo, colNo, my_dict['设备编号'][eqpCount])
        elif rowNo%6 == 1:
            if colNo%3 == 0:
                tarSheet.write(rowNo, colNo, '业务名称/IP')
            if colNo%3 == 1:
                busCount += 1
                tarSheet.write(rowNo, colNo, my_dict['业务名称/IP'][busCount])
        elif rowNo%6 == 2:
            if colNo % 3 == 0:
                tarSheet.write(rowNo, colNo, '单位/部门')
            if colNo%3 == 1:
                depaCount += 1
                tarSheet.write(rowNo, colNo, my_dict['单位/部门'][depaCount])
        elif rowNo%6 == 3:
            if colNo % 3 == 0:
                tarSheet.write(rowNo, colNo, '物理资源')
            if colNo%3 == 1:
                phyCount += 1
                tarSheet.write(rowNo, colNo, my_dict['物理资源'][phyCount])
        elif rowNo%6 == 4:
            if colNo % 3 == 0:
                tarSheet.write(rowNo, colNo, '备注')
            if colNo%3 == 1:
                noteCount += 1
                tarSheet.write(rowNo, colNo, my_dict['备注'][noteCount])
        else:
            pass
tarFile.save('c.xls')


