# coding:utf-8
import xlrd
import pandas
from xlwt import *



class ExcelHandler():
    def __init__(self, file):
        self.file = file

    # 获取所有的sheet页名称
    def get_sheet_name(self):
        readbook = pandas.ExcelFile(self.file)
        sheetName = readbook.sheet_names
        return sheetName

    # 获取sheet页内容
    def open_sheet(self, name):
        readbook = xlrd.open_workbook(self.file)
        sheet = readbook[name]
        return sheet

    # 创建一个新表
    def createExcel(self):
        file = Workbook(encoding='utf-8')
        return file

    # 给表创建sheet页
    def createSheet(self, file, name):
        sheet = file.add_sheet(name)

    # 保存表
    def saveExcel(self, file, fname):
        file.save(fname)

    # 获取最大行数
    def getRowMax(self, name):
        sheet = self.open_sheet(name)
        rowsMax = sheet.nrows
        return rowsMax

    # 获取最大列数
    def getColMax(self, name):
        sheet = self.open_sheet(name)
        colsMax = sheet.ncols
        return colsMax

    # 读取指定行列数据
    def readExcel(self, row, col, name):
        sheet = self.open_sheet(name)
        cellValue = sheet.cell_value(row,col)
        return cellValue

    # 在指定行列写入数据
    def writeExcel(self, row, col, name, value):
        sheet = self.open_sheet(name)
        sheet.write(row, col, value)
